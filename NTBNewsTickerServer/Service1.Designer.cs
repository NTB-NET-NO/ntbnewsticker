﻿namespace NTBNewsTickerServer
{
    partial class NewsTickerService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTimer = new System.Timers.Timer();
            this.fileSystemWatcher = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.mainTimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher)).BeginInit();
            // 
            // mainTimer
            // 
            this.mainTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.mainTimer_Elapsed);
            // 
            // fileSystemWatcher
            // 
            this.fileSystemWatcher.Filter = "HeartBeat.txt";
            this.fileSystemWatcher.Path = "C:\\Utvikling\\NTBNewsTicker\\HeartBeatFile";
            this.fileSystemWatcher.Changed += new System.IO.FileSystemEventHandler(this.fileSystemWatcher_Changed);
            // 
            // NewsTickerService
            // 
            this.ServiceName = "Service1";
            ((System.ComponentModel.ISupportInitialize)(this.mainTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher)).EndInit();

        }

        #endregion

        private System.Timers.Timer mainTimer;
        private System.IO.FileSystemWatcher fileSystemWatcher;
    }
}
