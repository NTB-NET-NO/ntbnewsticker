﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using log4net;
using log4net.Config;
using System.Net.Sockets;
using System.Net;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace NTBNewsTickerServer
{
    public partial class DebugForm : Form
    {
        private DateTime lastUpdate;
        private Socket udpServerSocket;
        private EndPoint ep = new IPEndPoint(IPAddress.Any, 10000);

        private byte[] buffer = new byte[1024];

        /// <summary>
        /// Setting up logger object
        /// </summary>
        private static ILog logger;

        private Process[] procs;

        private Process Client;

        private string ProcessName = ConfigurationManager.AppSettings["ProcessName"];

        private string HeartBeatType = ConfigurationManager.AppSettings["HeartBeatType"];

        private string HeartBeatFile = ConfigurationManager.AppSettings["HeartBeatFile"];

        public DebugForm()
        {

            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!log4net.LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();

            logger = LogManager.GetLogger(typeof(NewsTickerService));


            if (ConfigurationManager.AppSettings["HeartBeatType"].ToUpper() != "FILE")
            {
                udpServerSocket = new Socket(AddressFamily.InterNetwork,
                    SocketType.Dgram, ProtocolType.Udp);
                udpServerSocket.Bind(ep);
                udpServerSocket.BeginReceiveFrom(buffer, 0, 1024, SocketFlags.None, ref ep, new AsyncCallback(ReceiveData), udpServerSocket);
            }

            
            InitializeComponent();
        }

        private void CheckHeartBeatFile()
        {
            FileInfo fi = new FileInfo(HeartBeatFile + "heartbeat.txt");

            if (fi.Exists)
            {
                lastUpdate = fi.LastWriteTime;
            }
        }

        private void DebugForm_Load(object sender, EventArgs e)
        {
            procs = Process.GetProcessesByName(ProcessName);

            // Check fileage of heartbeat
            if (HeartBeatType.ToUpper() == "FILE")
            {
                CheckHeartBeatFile();
            }


            if (procs.Length > 0)
            {

                Client = procs[0];

                mainTimer.Interval = 30 * 1000;
                mainTimer.Enabled = true;
                mainTimer.Start();
            }
            else
            {
                logger.Fatal("Process not found. We are quitting!");
                mainTimer.Enabled = false;
                mainTimer.Stop();
                // Stopping the program if there is no process by the name of NTB NewsTicker
                this.Dispose();
                this.Close();
            }

            fileSystemWatcher.EnableRaisingEvents = true;

            logger.Info("===================================================");
            logger.Info("===      NTB NewsTicker Server has started      ===");
            logger.Info("===================================================");
        }

        private void mainTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (Client == null) 
            {
                logger.Info(ProcessName + " has exited or has not been started. Stopping.");
                logger.Info(ProcessName + " has to be started before the service is started");

                this.Dispose();
                this.Close();
                
            }
            int MaxTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxTimeOut"]);
            string ClientName = ConfigurationManager.AppSettings["ClientName"];
            // Calculate the Timespan since the Last Update from the Client.
            TimeSpan timeSinceLastHeartbeat = DateTime.Now.ToUniversalTime() - lastUpdate;

            // Set Lable Text depending of the Timespan
            if (timeSinceLastHeartbeat > TimeSpan.FromSeconds(MaxTimeOut))
            {


                if (Client != null)
                {
                    
                    if (!Client.HasExited)
                    {
                        var ClientRestart = Client.MainModule.FileName;
                        Client.Kill();

                        Process.Start(ClientRestart);

                    }
                }
            }
            else
            {
                // Really doing nothing
            }
        }

        void ReceiveData(IAsyncResult iar)
        {
            // Create temporary remote end Point
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            EndPoint tempRemoteEP = (EndPoint)sender;

            // Get the Socket
            Socket remote = (Socket)iar.AsyncState;

            // Call EndReceiveFrom to get the received Data
            int recv = remote.EndReceiveFrom(iar, ref tempRemoteEP);

            // Get the Data from the buffer to a string
            string stringData = Encoding.ASCII.GetString(buffer, 0, recv);

            // update Timestamp
            lastUpdate = DateTime.Now.ToUniversalTime();

            // Restart receiving
            udpServerSocket.BeginReceiveFrom(buffer, 0, 1024, SocketFlags.None, ref ep, new AsyncCallback(ReceiveData), udpServerSocket);

        }

        private void fileSystemWatcher_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            // update Timestamp
            lastUpdate = DateTime.Now.ToUniversalTime();

        }
    }
}
