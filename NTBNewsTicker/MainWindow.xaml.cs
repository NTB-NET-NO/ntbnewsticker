﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Threading;
using System.Windows.Shapes;
using System.Configuration;
using System.Net;
using System.Xml;
using System.ComponentModel;
using System.Diagnostics;
using System.Timers;

// Adding support for log4net
using log4net;
using log4net.Repository;
using log4net.Appender;
using System.Net.Sockets;
using System.IO;
using System.Collections;


namespace NTBNewsTicker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public string UserName = ConfigurationManager.AppSettings["RSSUserName"];

        public string PassWord = ConfigurationManager.AppSettings["RSSPassWord"];
        public string ServerName = ConfigurationManager.AppSettings["RSSServer"];
        public string FileName = ConfigurationManager.AppSettings["RSSFilename"];

        public string TickerSpeed = ConfigurationManager.AppSettings["Speed"];

        TextBlock myTextBlock = new TextBlock();
        TextBlock myTitleBlock = new TextBlock();
        TextBlock myRunBlock = new TextBlock();

        ContentControl myTitleContentControl = new ContentControl();
        ContentControl myTextContentControl = new ContentControl();

        System.Timers.Timer myHeartBeatTimer = new System.Timers.Timer();

        DispatcherTimer dispatcherTimer = new DispatcherTimer();

        XmlNodeList xmlNodes;

        int nodes;

        private string HeartBeatType = ConfigurationManager.AppSettings["HeartBeat"]; // ConfigurationManager.AppSettings["HeartBeat"];

        private string HeartBeatFile = ConfigurationManager.AppSettings["HeartBeatFile"];// ConfigurationManager.AppSettings["HeartBeatFile"];

        string ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location;

        bool busy = false;

        log4net.ILog logger = LogManager.GetLogger("MainWindow");



        public MainWindow()
        {
            logger.Info("Main Window. Initializing Component!");


            InitializeComponent();

            this.PreviewKeyDown += new KeyEventHandler(HandleEsc);
        }

        void HandleEsc(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                dispatcherTimer.IsEnabled = false;
                dispatcherTimer.Stop();

                myHeartBeatTimer.Enabled = false;
                myHeartBeatTimer.Stop();
                Close();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                logger.Debug("Window_Loaded!");

                // Checking if directory exists, and if it doesn't create it
                if (!Directory.Exists(HeartBeatFile))
                {
                    Directory.CreateDirectory(HeartBeatFile);
                }
                if (HeartBeatType == "File")
                {
                    UpdateFile();
                }

                logger.Debug("Placement of Application: " + ApplicationPath);

                logger.Debug("Setting up HeartBeat timer");


                // Creating an ElapsedEventHandler for HeartBeat Timer
                logger.Debug("Creating an ElapsedEventHandler for HeartBeat Timer");
                myHeartBeatTimer.Elapsed += new ElapsedEventHandler(myHeartBeatTimer_Elapsed);


                logger.Debug("Interval set to 30 * 1000" + 30 * 1000);
                myHeartBeatTimer.Interval = 30 * 1000; // Every half minute

                logger.Debug("Enabling HeartBeat Timer");
                myHeartBeatTimer.Enabled = true;

                logger.Debug("Starting HeartBeat Timer");
                myHeartBeatTimer.Start();

            }
            catch (Exception exception)
            {
                logger.Info(exception.Message);
                logger.Info(exception.StackTrace.ToString());
            }

        }

        void myHeartBeatTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            logger.Debug("In myHeartBeatTimer_Elapsed");

            logger.Debug("Calling SendUdpPacket");

            if (HeartBeatType == "TCP")
            {
                SendUdpPacket();
            }
            else
            {
                UpdateFile();
            }
        }

        /// <summary>
        /// Method that updates content in a file
        /// </summary>
        private void UpdateFile()
        {
            // Setting content of file 
            string FileContent = ConfigurationManager.AppSettings["ApplicationName"];
            // ConfigurationManager.AppSettings["ApplicationName"];

            string FilePlacement = ConfigurationManager.AppSettings["HeartBeatFile"];

            // Setting filename
            string FileName = "HeartBeat.txt";

            // Now we shall write the file
            // Write the string to a file.
            System.IO.StreamWriter file = new System.IO.StreamWriter(FilePlacement + FileName);
            file.WriteLine(FileContent);

            file.Close();

        }

        private void SendUdpPacket()
        {
            try
            {
                logger.Debug("In SendUdpPacket");
                IPEndPoint ipep = new IPEndPoint(IPAddress.Loopback, 10000);

                byte[] data = new byte[1024];

                // Create UDP Socket
                Socket udpClientSocket = new Socket(AddressFamily.InterNetwork,
                                    SocketType.Dgram,
                                    ProtocolType.Udp);

                // Send Application Title (Window Title) as the Data
                string AppName = ConfigurationManager.AppSettings["ApplicationName"];
                // ConfigurationManager.AppSettings["ApplicationName"];
                data = Encoding.ASCII.GetBytes(AppName);

                // Send it ...
                logger.Debug("Send the UDP Packet");
                udpClientSocket.SendTo(data, 0, data.Length, SocketFlags.None, ipep);
            }
            catch (Exception exception)
            {
                logger.Error(exception.Message);
                logger.Error(exception.StackTrace.ToString());
            }
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {


            try
            {

                if (busy == false)
                {
                    busy = true;
                    WebClient client = new WebClient();
                    string RSSFile = ServerName + FileName;
                    try
                    {
                        client.Credentials = new NetworkCredential(UserName, PassWord);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Something went wrong while getting access to RSSFeed\r\n" + ex.Message);
                    }

                    // Can we read directly from the RSS-feed-file, or must we dowload it like I have done in the past?
                    XmlTextReader reader = new XmlTextReader(RSSFile);
                    XmlDocument xmlDoc = new XmlDocument();

                    try
                    {
                        xmlDoc.Load(reader);
                    }
                    catch (XmlException xmlexception)
                    {

                        logger.Debug(xmlexception.StackTrace.ToString());
                        logger.Debug(xmlexception.Message.ToString());
                    }
                    catch (Exception exception)
                    {
                        logger.Debug(exception.StackTrace.ToString());
                        logger.Debug(exception.Message.ToString());
                    }

                    XmlNodeList xmlNodes = xmlDoc.SelectNodes("//channel/item");

                    int Counter = xmlNodes.Count;


                    for (int nodes = 0; nodes < Counter; nodes++)
                    {


                        MyTitle myTitle = new MyTitle(xmlNodes.Item(nodes).SelectSingleNode("title").InnerText);


                        MyText myText = new MyText(xmlNodes.Item(nodes).SelectSingleNode("description").InnerText);


                        Run myTitleRun = new Run(myTitle.MyTitleProperty + "\r\n");
                        myTitleRun.FontSize = 52;
                        myTitleRun.FontFamily = new FontFamily("Replica");
                        myTitleRun.Foreground = Brushes.Black;

                        Run myTextRun = new Run(myText.MyTextProperty);
                        myTextRun.FontSize = 36;
                        myTextRun.FontFamily = new FontFamily("Replica");
                        myTextRun.Foreground = Brushes.Black;

                        myRunBlock.Text = "";
                        myRunBlock.Inlines.Clear();

                        myRunBlock.Inlines.Add(myTitleRun);
                        myRunBlock.Inlines.Add(myTextRun);

                        //myRunBlock.Dispatcher.BeginInvoke(
                        //    new Action(() => myRunBlock.Text = myTitle.MyTitleProperty + @"\r\n" + myText.MyTextProperty));
                        DoEvents();

                        int speed = Convert.ToInt32(ConfigurationManager.AppSettings["Speed"]) * 1000;

                        int speeddivider = 6;
                        int speedparts = speed / speeddivider;

                        for (int i = 0; i < speeddivider; i++)
                        {
                            Thread.Sleep(speedparts);
                        }
                        // UpdateFile();
                        

                        // 
                    }

                    busy = false;
                }
            }
            catch (Exception exception)
            {
                logger.Debug(exception.StackTrace.ToString());
                logger.Debug(exception.Message.ToString());
            }

        }

        private delegate void EmptyDelegate();


        public static void DoEvents()
        {
            Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Background, new EmptyDelegate(delegate { }));
        }




        private void MainCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                // Application path for the program
                logger.Debug("Setting up Application path for the program");
                string appPath = ConfigurationManager.AppSettings["ApplicationDirectory"];
                logger.Debug("Application path for the program is: " + appPath);

                logger.Debug("Setting up brushes and stuff");
                BrushConverter myBrushConverter = new BrushConverter();
                var myBrush = myBrushConverter.ConvertFromString("#FFFFFFFF") as Brush;

                // myColor = "#FF71BABE";
                var myBackgroundBrush = myBrushConverter.ConvertFromString("#FFFFFFFF") as Brush;

                logger.Debug("Setting canvas to fill whole program");
                MainCanvas.Width = this.Width;
                MainCanvas.Height = this.Height;
                MainCanvas.Background = myBackgroundBrush;

                // Creating the top canvas that shall also contain the LIVE text
                Canvas canvasTop = new Canvas();
                canvasTop.Width = this.ActualWidth;
                canvasTop.Height = (int)this.ActualHeight / 6;
                canvasTop.Background = myBrush;
                Canvas.SetTop(canvasTop, 0);
                Canvas.SetLeft(canvasTop, 0);

                Canvas canvasLive = new Canvas();
                canvasLive.SetValue(Canvas.ZIndexProperty, 1);
                Canvas.SetLeft(canvasLive, this.Width - 300);
                Canvas.SetTop(canvasLive, 25);


                TextBlock myLiveBlock = new TextBlock();

                myLiveBlock.Text = "LIVE";
                myLiveBlock.FontSize = (int)96;
                myLiveBlock.FontFamily = new FontFamily("Replica");
                myLiveBlock.Foreground = myBrushConverter.ConvertFromString("#FFC95226") as Brush;
                myLiveBlock.VerticalAlignment = VerticalAlignment.Top;
                myLiveBlock.HorizontalAlignment = HorizontalAlignment.Right;
                canvasLive.Children.Add(myLiveBlock);



                canvasTop.Children.Add(canvasLive);

                // Now to place the logo
                string LogoPath = appPath + @"\Images\NTBnyheter_logo_RGB.png";
                Canvas canvasLogo = new Canvas();
                canvasLogo.SetValue(Canvas.ZIndexProperty, 2);

                logger.Debug("Creating logo");
                try
                {
                    Image myLogoImage = new Image();
                    BitmapImage myLogoBitmapImage = new BitmapImage();
                    myLogoBitmapImage.BeginInit();
                    myLogoBitmapImage.UriSource = new Uri(LogoPath, UriKind.Absolute);
                    myLogoBitmapImage.EndInit();
                    myLogoImage.Source = myLogoBitmapImage;

                    Canvas.SetTop(canvasLogo, 250);
                    Canvas.SetLeft(canvasLogo, 250);

                    canvasLogo.Children.Add(myLogoImage);
                }
                catch (Exception exception)
                {
                    logger.Fatal("Cannot load image " + LogoPath);
                    logger.Fatal(exception.Message);
                    logger.Fatal(exception.StackTrace.ToString());

                }


                // Now let's do the background graphics
                logger.Debug("Creating background graphics");
                Canvas canvasBackgroundGraphics = new Canvas();
                canvasBackgroundGraphics.SetValue(Canvas.ZIndexProperty, 2);
                string BackgroundGraphicsPath = appPath + @"\Images\krystallen.png";
                try
                {
                    Image myBackgroundImage = new Image();
                    BitmapImage myBackgroundBitmapImage = new BitmapImage();
                    myBackgroundBitmapImage.BeginInit();
                    myBackgroundBitmapImage.UriSource = new Uri(BackgroundGraphicsPath, UriKind.Absolute);
                    myBackgroundBitmapImage.EndInit();

                    myBackgroundImage.Source = myBackgroundBitmapImage;

                    //RotateTransform myRotation = new RotateTransform(45);
                    //myBackgroundImage.RenderTransform = myRotation;

                    Canvas.SetTop(canvasBackgroundGraphics, -300);
                    Canvas.SetLeft(canvasBackgroundGraphics, -300);

                    canvasBackgroundGraphics.Children.Add(myBackgroundImage);
                }
                catch (Exception exception)
                {
                    logger.Fatal("Cannot load image " + BackgroundGraphicsPath);
                    logger.Fatal(exception.Message);
                    logger.Fatal(exception.StackTrace.ToString());
                }



                logger.Debug("Creating background stripy crystals");
                // Canvas for the stripy crystals
                string BackgroundStripyCrystalsPath = appPath + @"\Images\graa_krystall.png";
                Canvas canvasBackgroundStripyGraphics = new Canvas();
                canvasBackgroundStripyGraphics.SetValue(Canvas.ZIndexProperty, 2);
                try
                {
                    Image myCrystalsImage = new Image();
                    BitmapImage myBackgroundStripyCrystal = new BitmapImage();
                    myBackgroundStripyCrystal.BeginInit();
                    myBackgroundStripyCrystal.UriSource = new Uri(BackgroundStripyCrystalsPath, UriKind.Absolute);
                    myBackgroundStripyCrystal.EndInit();

                    myCrystalsImage.Source = myBackgroundStripyCrystal;
                    var test = MainCanvas.Height - myCrystalsImage.Source.Height;
                    Canvas.SetTop(canvasBackgroundStripyGraphics, MainCanvas.Height - myCrystalsImage.Source.Height);
                    Canvas.SetLeft(canvasBackgroundStripyGraphics, MainCanvas.Width - myCrystalsImage.Source.Width);


                    canvasBackgroundStripyGraphics.Children.Add(myCrystalsImage);
                }
                catch (Exception exception)
                {
                    logger.Fatal("Cannot load image " + BackgroundStripyCrystalsPath);
                    logger.Fatal(exception.Message);
                    logger.Fatal(exception.StackTrace.ToString());

                }




                // Placing the elements
                logger.Debug("Placing the elements");
                MainCanvas.Children.Add(canvasLogo);
                MainCanvas.Children.Add(canvasBackgroundGraphics);
                MainCanvas.Children.Add(canvasBackgroundStripyGraphics);
                MainCanvas.Children.Add(canvasTop);

                logger.Debug("Setting title to NTB nyheter");
                MyTitle myTitle = new MyTitle("NTB nyheter");

                // Binding the Title to the MyTitle object
                Binding titleBind = new Binding("MyTitleProperty");
                titleBind.Source = myTitle;
                titleBind.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                myTitleBlock.SetBinding(TextBlock.TextProperty, titleBind);

                Run myTitleRun = new Run(myTitle.MyTitleProperty);
                myTitleRun.FontSize = 52;
                myTitleRun.FontWeight = FontWeights.Bold;
                myTitleRun.FontFamily = new FontFamily("Replica");
                myTitleRun.Foreground = Brushes.Black;
                myTitleRun.Name = "myTitleBlock";
                myTitleRun.Text = "NTB nyheter\r\n";
                // myTitleRun.TextWrapping = TextWrapping.Wrap;


                MyText myText = new MyText("Vennligst vent mens vi henter nyheter");
                Binding textBind = new Binding("MyTextProperty");
                textBind.Source = myText;
                textBind.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;

                myTextBlock.SetBinding(TextBlock.TextProperty, textBind);

                Run myTextRun = new Run(myText.MyTextProperty);
                myTextRun.FontSize = 36;
                myTextRun.FontFamily = new FontFamily("Replica");
                myTextRun.Foreground = Brushes.Black;

                myRunBlock.Inlines.Add(myTitleRun);
                myRunBlock.Inlines.Add(myTextRun);
                myRunBlock.TextWrapping = TextWrapping.Wrap;

                myTextContentControl.Content = myRunBlock;
                myTextContentControl.Width = this.ActualWidth - 600;


                //Canvas.SetTop(myTextContentControl, 350);
                //Canvas.SetLeft(myTextContentControl, 410);
                Canvas.SetZIndex(myTextContentControl, 99);

                Canvas.SetTop(myTextContentControl, 410);
                Canvas.SetLeft(myTextContentControl, 416);

                myTextContentControl.Content = myRunBlock;
                myTextContentControl.Width = this.ActualWidth - 600;

                // Adding the title to the canvas
                MainCanvas.Children.Add(myTextContentControl);

                // Now that we have set up the form. We can do the rest
                logger.Debug("Setting up dispatcherTimer_Tick event");
                int speed = Convert.ToInt32(ConfigurationManager.AppSettings["Speed"]);
                TimeSpan ts = new TimeSpan(0, 0, speed);
                dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
                dispatcherTimer.Interval = ts;
                dispatcherTimer.IsEnabled = true;
                dispatcherTimer.Start();

                // DoNewsTicker();
            }
            catch (Exception exception)
            {
                logger.Debug(exception.StackTrace.ToString());
                logger.Debug(exception.Message.ToString());
            }
        }

        //private void DoNewsTicker(bool restart = false)
        //{

        //    if (busy == false)
        //    {
        //        busy = true;
        //        WebClient client = new WebClient();
        //        string RSSFile = ServerName + FileName;
        //        try
        //        {
        //            client.Credentials = new NetworkCredential(UserName, PassWord);
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show("Something went wrong while getting access to RSSFeed\r\n" + ex.Message);
        //        }

        //        // Can we read directly from the RSS-feed-file, or must we dowload it like I have done in the past?
        //        XmlTextReader reader = new XmlTextReader(RSSFile);
        //        XmlDocument xmlDoc = new XmlDocument();

        //        try
        //        {
        //            xmlDoc.Load(reader);
        //        }
        //        catch (XmlException xmlexception)
        //        {

        //            logger.Debug(xmlexception.StackTrace.ToString());
        //            logger.Debug(xmlexception.Message.ToString());
        //        }
        //        catch (Exception exception)
        //        {
        //            logger.Debug(exception.StackTrace.ToString());
        //            logger.Debug(exception.Message.ToString());
        //        }

        //        xmlNodes = xmlDoc.SelectNodes("//channel/item");

        //        int nodes = 0;
        //        int Counter = xmlNodes.Count - 1;

        //        // new EventHandler(dispatcherTimer_Tick);
        //        int Seconds = Convert.ToInt32(ConfigurationManager.AppSettings["Speed"]);


        //        dispatcherTimer.Interval =
        //            new TimeSpan(0, 0, Seconds);
        //        logger.Debug("Starting dispatcherTimer");

        //        do
        //        {
        //            // I won't do a thing here... 
        //        } while (busy == false);

        //        dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
        //        dispatcherTimer.Start();
        //        //dispatcherTimer.Tick += (s, e) =>
        //        //{

        //        //    MyTitle myTitle = new MyTitle();
        //        //    MyText myText = new MyText();

        //        //    if (xmlNodes != null)
        //        //    {
        //        //        myTitle.MyTitleProperty = xmlNodes.Item(nodes).SelectSingleNode("title").InnerText;
        //        //        myText.MyTextProperty = xmlNodes.Item(nodes).SelectSingleNode("description").InnerText;
        //        //    }

        //        //    Run myTitleRun = new Run(myTitle.MyTitleProperty + "\r\n");
        //        //    myTitleRun.FontSize = 52;
        //        //    myTitleRun.FontFamily = new FontFamily("Replica");
        //        //    myTitleRun.Foreground = Brushes.Black;

        //        //    Run myTextRun = new Run(myText.MyTextProperty);
        //        //    myTextRun.FontSize = 36;
        //        //    myTextRun.FontFamily = new FontFamily("Replica");
        //        //    myTextRun.Foreground = Brushes.Black;

        //        //    myRunBlock.Text = "";

        //        //    myRunBlock.Inlines.Clear();

        //        //    myRunBlock.Inlines.Add(myTitleRun);
        //        //    myRunBlock.Inlines.Add(myTextRun);

        //        //    nodes++;
        //        //    if (nodes > Counter)
        //        //    {
        //        //        nodes = 0;
        //        //        dispatcherTimer.Stop();
        //        //        busy = false;
        //        //        DoNewsTicker(true);

        //        //    }
        //        //};


        //    }


    }


    public class MyTitle : INotifyPropertyChanged
    {
        private string myTitleProperty;

        public MyTitle() { }

        public MyTitle(string Title)
        {
            if (Title.Length > 50)
            {
                // Title = Title.Substring(0, 50) + "\r\n" + Title.Substring(51);

            }
            myTitleProperty = Title;
        }

        public String MyTitleProperty
        {
            get { return myTitleProperty; }
            set
            {
                myTitleProperty = value;
                OnPropertyChanged("MyTitleProperty");
            }
        }

        public int getLength()
        {
            return myTitleProperty.Length;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }
    }

    public class MyText : INotifyPropertyChanged
    {
        private string myTextProperty;

        public MyText() { }

        public MyText(string Title)
        {
            myTextProperty = Title;
        }

        public String MyTextProperty
        {
            get { return myTextProperty; }
            set
            {
                myTextProperty = value;
                OnPropertyChanged("MyTextProperty");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
