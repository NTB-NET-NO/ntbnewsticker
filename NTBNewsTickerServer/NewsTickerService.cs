﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Configuration;

// Adding support for log4net
using log4net;
using log4net.Config;
using System.IO;


namespace NTBNewsTickerServer
{
    public partial class NewsTickerService : ServiceBase
    {
        private DateTime lastUpdate;
        private Socket udpServerSocket;
        private EndPoint ep = new IPEndPoint(IPAddress.Any, 10000);

        private byte[] buffer = new byte[1024];

        /// <summary>
        /// Setting up logger object
        /// </summary>
        private static ILog logger;

        private Process[] procs;

        private Process Client;

        private string ProcessName = ConfigurationManager.AppSettings["ProcessName"];

        private string HeartBeatType = ConfigurationManager.AppSettings["HeartBeatType"];

        private string HeartBeatFile = ConfigurationManager.AppSettings["HeartBeatFile"];

        public NewsTickerService()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!log4net.LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();

            logger = LogManager.GetLogger(typeof(NewsTickerService));

            if (HeartBeatType == "TCP")
            {
                udpServerSocket = new Socket(AddressFamily.InterNetwork,
                    SocketType.Dgram, ProtocolType.Udp);
                udpServerSocket.Bind(ep);
                udpServerSocket.BeginReceiveFrom(buffer, 0, 1024, SocketFlags.None, ref ep, new AsyncCallback(ReceiveData), udpServerSocket);
            }
            else
            {
                // We are checking for directory exists, but that should have been 
                // started with the main program.
                if (!Directory.Exists(HeartBeatFile))
                {
                    // So that's we are then stopping the program.
                    logger.Fatal("Directory " + HeartBeatFile + " does not exist. We are stopping");
                    mainTimer.Enabled = false;
                    mainTimer.Stop();

                    this.Stop();

                }
            }


            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            procs = Process.GetProcessesByName(ProcessName);

            if (procs.Length > 0)
            {

                Client = procs[0];

                mainTimer.Interval = 30 * 1000;
                mainTimer.Enabled = true;
                mainTimer.Start();
            }
            else
            {
                logger.Fatal("Process not found. We are quitting!");
                mainTimer.Enabled = false;
                mainTimer.Stop();
                // Stopping the program if there is no process by the name of NTB NewsTicker
                this.Dispose();
                this.Stop();
            }

            
            // Check fileage of heartbeat
            if (HeartBeatType.ToUpper() == "FILE")
            {
                CheckHeartBeatFile();

                fileSystemWatcher.EnableRaisingEvents = true;
            }

            mainTimer.Interval = 30 * 1000;
            mainTimer.Enabled = true;
            mainTimer.Start();


            logger.Info("===================================================");
            logger.Info("===      NTB NewsTicker Server has started      ===");
            logger.Info("===================================================");
        }

        private void CheckHeartBeatFile()
        {
            FileInfo fi = new FileInfo(HeartBeatFile + "heartbeat.txt");

            if (fi.Exists)
            {
                lastUpdate = fi.LastWriteTime;
            }
        }

        protected override void OnStop()
        {
            mainTimer.Stop();
            mainTimer.Enabled = false;

            logger.Info("===================================================");
            logger.Info("===      NTB NewsTicker Server has stopped      ===");
            logger.Info("===================================================");
        }

        private void mainTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            logger.Debug("In mainTimer_Elapsed");
            if (Client == null)
            {
                logger.Info(ProcessName + " has exited or has not been started. Stopping.");
                logger.Info(ProcessName + " has to be started before the service is started");

                this.Dispose();
                this.Stop();

            }
            int MaxTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxTimeOut"]);
            logger.Debug("MaxTimeOut set to: " + MaxTimeOut.ToString());

            string ClientName = ConfigurationManager.AppSettings["ClientName"];
            logger.Debug("ClientName set to: " + ClientName);

            // Calculate the Timespan since the Last Update from the Client.
            TimeSpan timeSinceLastHeartbeat = DateTime.Now.ToUniversalTime() - lastUpdate;
            logger.Debug("Time since last heartbeat: " + timeSinceLastHeartbeat.ToString());

            // Set Lable Text depending of the Timespan
            if (timeSinceLastHeartbeat > TimeSpan.FromSeconds(MaxTimeOut))
            {

                procs = Process.GetProcessesByName("NTBNewsTicker");

                Client = procs[0];
                logger.Debug("Got Client. " + Client.ProcessName.ToString());

                string ClientRestart = Client.MainModule.FileName;
                logger.Debug("ClientRestart is: " + ClientRestart);
                if (Client.HasExited)
                {
                    logger.Debug("Client has exited. Trying to restart");

                    logger.Info("Killing Client");
                    Client.Kill();

                    logger.Info("Restarting Client");
                    Process.Start(ClientRestart);

                }
            }
        }

        void ReceiveData(IAsyncResult iar)
        {
            logger.Debug("In ReceiveData");
            // Create temporary remote end Point
            logger.Debug("Create temporary remote end Point");
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            EndPoint tempRemoteEP = (EndPoint)sender;

            // Get the Socket
            logger.Debug("Get the Socket");
            Socket remote = (Socket)iar.AsyncState;

            // Call EndReceiveFrom to get the received Data
            logger.Debug("Call EndReceiveFrom to get the received Data");
            int recv = remote.EndReceiveFrom(iar, ref tempRemoteEP);

            // Get the Data from the buffer to a string
            logger.Debug("Get the Data from the buffer to a string");
            string stringData = Encoding.ASCII.GetString(buffer, 0, recv);

            // update Timestamp
            logger.Debug("update Timestamp");
            lastUpdate = DateTime.Now.ToUniversalTime();

            // Restart receiving
            logger.Debug("Restart receiving");
            udpServerSocket.BeginReceiveFrom(buffer, 0, 1024, SocketFlags.None, ref ep, new AsyncCallback(ReceiveData), udpServerSocket);

        }

        private void fileSystemWatcher_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            // update Timestamp
            lastUpdate = DateTime.Now.ToUniversalTime();

        }
    }
}
