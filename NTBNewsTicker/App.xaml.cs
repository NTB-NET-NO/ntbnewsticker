﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Diagnostics;

using log4net;
using log4net.Config;

namespace NTBNewsTicker
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static ILog log;

        static App()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!log4net.LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();

            log = LogManager.GetLogger(typeof(App));

        }
    }
}
